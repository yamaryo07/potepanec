FactoryBot.define do
  factory :ex_product, class: "Spree::Product" do
    shipping_category { |r| Spree::ShippingCategory.first || r.association(:shipping_category) }
    sequence :name do |n|
      "product#{n}"
    end
    price { 15.55 }

    trait :bag do
      sequence :name do |n|
        "bag#{n}"
      end
      price { 30.45 }
    end

    trait :mug do
      sequence :name do |n|
        "mug#{n}"
      end
      price { 22.45 }
    end
  end
end
