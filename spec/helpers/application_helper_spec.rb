require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full title helpers" do
    context "when title is nil" do
      it "provide the default title" do
        expect(full_title(page_title: nil)).to eq ApplicationHelper::BASE_TITLE
      end
    end

    context "when title is brank" do
      it "provide the default title" do
        expect(full_title(page_title: "")).to eq ApplicationHelper::BASE_TITLE
      end
    end

    context "when title is presence" do
      it "provide the (title) - BASE_TITLE" do
        expect(full_title(page_title: "example")).to eq "example - #{ApplicationHelper::BASE_TITLE}"
      end
    end
  end
end
