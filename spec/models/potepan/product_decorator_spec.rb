require 'rails_helper'

describe "Spree::Product", type: :model do
  describe "related_products" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product_1) { create(:product, taxons: [taxon]) }
    let!(:related_product_2) { create(:product, taxons: [taxon]) }
    let!(:not_related_product) { create(:product) }
    let!(:related_products) { product.related_products }

    it "find same taxons products" do
      expect(related_products).to include(related_product_1)
      expect(related_products).to include(related_product_2)
      expect(related_products).not_to include(product)
    end

    it "dose not find other taxons products" do
      expect(related_products).not_to include(not_related_product)
    end
  end
end
