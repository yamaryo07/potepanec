require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:categories) { create(:taxonomy, name: "Categories") }
  let(:categories_t) { Spree::Taxon.find_by(name: 'Categories') }
  let!(:bags) { create(:taxon, name: "Bags", taxonomy: categories, parent_id: categories_t.id) }
  let!(:mugs) { create(:taxon, name: "Mugs", taxonomy: categories, parent_id: categories_t.id) }

  describe "Page Layout" do
    let!(:bags_products) { create_list(:ex_product, 4, :bag, taxons: [bags]) }
    let!(:mugs_products) { create_list(:ex_product, 6, :mug, taxons: [mugs]) }

    scenario "show Categories page" do
      visit potepan_category_path(categories_t.id)
      expect(page).to have_current_path potepan_category_path(categories_t.id)
      expect(page).to have_title "Categories - BIGBAG Store"
      expect(page).to have_selector ".side-nav > li", text: "Categories"
      expect(page).to have_link "Bags", href: potepan_category_path(bags.id)
      expect(page).to have_link "Mugs", href: potepan_category_path(mugs.id)
      expect(all(".collapseItem > li").size).to eq(2)
      expect(page).to have_selector ".productCaption h5", text: "bag1"
      expect(page).to have_selector ".productCaption h3", text: "$30.45"
      expect(page).to have_selector ".productCaption h5", text: "mug1"
      expect(page).to have_selector ".productCaption h3", text: "$22.45"
      expect(all(".productBox").size).to eq(10)
    end

    scenario "show Bags page" do
      visit potepan_category_path(bags.id)
      expect(page).to have_current_path potepan_category_path(bags.id)
      expect(page).to have_title "Bags - BIGBAG Store"
      expect(page).to have_link "Bags", href: potepan_category_path(bags.id)
      expect(page).to have_link "Mugs", href: potepan_category_path(mugs.id)
      expect(all(".collapseItem > li").size).to eq(2)
      expect(page).to have_selector ".productCaption h5", text: "bag5"
      expect(page).to have_selector ".productCaption h3", text: "$30.45"
      expect(page).not_to have_selector ".productCaption h5", text: "mug7"
      expect(page).not_to have_selector ".productCaption h3", text: "$22.45"
      expect(all(".productBox").size).to eq(4)
    end
  end

  describe "User interface" do
    let!(:bag_link) { create(:product, name: "bag_link", taxons: [bags]) }

    scenario "click correct link" do
      visit potepan_category_path(categories_t.id)
      click_on "Bags"
      expect(page).to have_current_path potepan_category_path(bags.id)
      click_on "bag_link"
      expect(page).to have_current_path potepan_product_path(bag_link.id)
    end
  end
end
