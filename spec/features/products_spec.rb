require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }

  describe "whole page layout" do
    scenario "show single product page" do
      visit potepan_product_path product.id
      expect(page).to have_current_path potepan_product_path(product.id)
      expect(page).to have_title "#{product.name} - BIGBAG Store"
      expect(page).to have_selector ".page-title h2", text: product.name
      expect(page).to have_selector ".breadcrumb li", text: product.name
      expect(page).to have_selector ".media-body h2", text: product.name
      expect(page).to have_selector ".media-body h3", text: product.display_price
      expect(page).to have_selector ".media-body p",  text: product.description
      expect(page).to have_link "Home", href: potepan_path
      expect(page).to have_link "一覧ページへ戻る", href: potepan_category_path(taxon.id)
      click_on "一覧ページへ戻る"
      expect(page).to have_current_path potepan_category_path(taxon.id)
    end
  end

  describe "part page layout" do
    let!(:link_product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:ex_product, 8, taxons: [taxon]) }
    let!(:not_related_product) { create(:product) }

    scenario "show correct related products" do
      visit potepan_product_path product.id
      expect(page).to have_selector ".productCaption h5", text: link_product.name
      expect(page).to have_selector ".productCaption h5", text: "product1"
      expect(page).to have_selector ".productCaption h5", text: "product2"
      expect(page).to have_selector ".productCaption h5", text: "product3"
      expect(page).not_to have_selector ".productCaption h5", text: not_related_product.name
      expect(page).not_to have_selector ".productCaption h5", text: "product4"
      expect(all(".productBox").size).to eq(4)
      click_on link_product.name
      expect(page).to have_current_path potepan_product_path(link_product.id)
    end
  end
end
