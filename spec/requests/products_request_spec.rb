require 'rails_helper'

RSpec.describe "Products_request", type: :request do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product_1) { create(:product, taxons: [taxon]) }
    let!(:related_product_2) { create(:product, taxons: [taxon]) }
    let!(:not_related_product) { create(:product) }

    before do
      get potepan_product_path(product.id)
    end

    it "responds successfully" do
      expect(response).to be_successful
      expect(response).to have_http_status "200"
    end

    it "show correct view" do
      expect(response).to render_template :show
    end

    it "show correct product name" do
      expect(response.body).to include product.name
      expect(response.body).to include related_product_1.name
      expect(response.body).to include related_product_2.name
      expect(response.body).not_to include not_related_product.name
    end
  end
end
