require 'rails_helper'

RSpec.describe "Categories_request", type: :request do
  describe "GET #show" do
    let(:taxonomy) { create(:taxonomy, name: "taxonomy") }
    let(:taxon) { create(:taxon, name: "taxon", taxonomy: taxonomy) }
    let!(:product) { create(:product, name: "product", price: 30.45, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "responds successfully" do
      expect(response).to be_successful
      expect(response).to have_http_status "200"
    end

    it "show correct view" do
      expect(response).to render_template :show
    end

    it "show correct page contents" do
      expect(response.body).to include taxonomy.name
      expect(response.body).to include taxon.name
      expect(response.body).to include product.name
      expect(response.body).to include product.price.to_s
    end
  end
end
