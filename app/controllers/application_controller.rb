class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  MAX_DISPLAY_RELATED_PRODUCTS = 4
end
